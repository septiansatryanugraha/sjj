<?php $this->load->view('_heading/_headerContent') ?>
<section class="content">
    <!-- style loading -->
    <div class="loading2"></div>
    <!-- -->
    <div class="box">
        <div class="box-header with-border" style="text-align: right;">
            <a class="klik" href="<?php echo site_url($menuName); ?>"><button class="btn btn-success" ><i class="glyphicon glyphicon-chevron-left"></i> Kembali ke Rincian <?php echo $judul; ?></button></a>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-sm-12">
                    <div class="nav-tabs-custom" id="newContain">
                        <form class="form-horizontal" id="form-tambah" method="POST">
                            <div class="box-body">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Nama </label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="name" id="name" aria-describedby="sizing-addon2">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Bank </label>
                                        <div class="col-sm-5">
                                            <select name="id_bank" class="form-control select-bank" id="id_bank" aria-describedby="sizing-addon2">
                                                <option></option>
                                                <?php foreach ($bank as $data) { ?>
                                                    <option value="<?php echo $data->id_bank; ?>"><?php echo $data->name; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">No Rekening </label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" name="account_number" id="account_number" aria-describedby="sizing-addon2">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Pemilik Rekening </label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="account_holder" id="account_holder" aria-describedby="sizing-addon2">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Harga Drum / Pcs </label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control number_only formatCurrency" name="drum_price" id="drum_price" style="text-align: right;" value="0" aria-describedby="sizing-addon2">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Harga Oli / Liter </label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control number_only formatCurrency" name="oli_price" id="oli_price" style="text-align: right;" value="0" aria-describedby="sizing-addon2">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Telepon/HP </label>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control" name="phone" id="phone" aria-describedby="sizing-addon2">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Alamat </label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="address" id="address" aria-describedby="sizing-addon2">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button name="simpan" id="simpan" type="button" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
                                <button type="reset" class="btn btn-warning btn-flat"><i class="fa fa-retweet"></i> Bersihkan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    //Format Currency
    $('.formatCurrency').on('focusin', function () {
        var x = $(this).val();
        $(this).val(accounting.unformat(x));
    });
    $('.formatCurrency').on('focusout', function () {
        var x = $(this).val();
        $(this).val(accounting.formatMoney(x));
    });

    //Number Format
    $(document).on('keypress', '.number_only', function (event) {
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $(document).on('keypress', '.number_decimal', function (event) {
        if ((event.which != 44 && event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    //Proses Controller logic ajax
    $("#simpan").click(function () {
        swal({
            title: "Simpan Data?",
            text: "Apakah Anda Yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Simpan",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        },
        function () {
            $(".confirm").attr('disabled', 'disabled');
            var data = $("#newContain>form").serialize();
            $.ajax({
                method: 'POST',
                beforeSend: function () {
                    $(".loading2").show();
                    $(".loading2").modal('show');
                },
                url: '<?php echo site_url('Master/Supplier/prosesAdd'); ?>',
                data: data,
            }).done(function (data) {
                var result = jQuery.parseJSON(data);
                if (result.status == 'berhasil') {
                    document.getElementById("form-tambah").reset();
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    save_berhasil();
                    setTimeout(location.reload.bind(location), 450);
                } else {
                    $(".loading2").hide();
                    $(".loading2").modal('hide');
                    swal("Peringatan", result.status, "warning");
                }
            })
        });
    });

    // untuk select2 ajak pilih tipe
    $(function () {
        $(".select-bank").select2({
            placeholder: " -- pilih bank -- "
        });
    });
</script>





