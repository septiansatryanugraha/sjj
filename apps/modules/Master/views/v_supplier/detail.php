<div class="col-md-12 well">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h3 style="display:block; text-align:center;"><i class="fa fa-location-arrow"></i> Detail <?php echo $judul; ?></h3>
    <div class="col-sm-12">
        <form class="form-horizontal" id="form-ubah" method="POST">
            <div class="box box-body">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Nama </label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="name" id="name" value="<?php echo $dataSupplier->name; ?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Bank </label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="name" id="name" value="<?php echo $bank; ?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">No Rekening </label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control number_only" name="account_number" id="account_number" value="<?php echo $dataSupplier->account_number; ?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Pemilik Rekening </label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="account_holder" id="account_holder" value="<?php echo $dataSupplier->account_holder; ?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Harga Drum / Pcs </label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control number_decimal formatCurrency" name="drum_price" id="drum_price" style="text-align: right;" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Harga Oli / Liter </label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control number_decimal formatCurrency" name="oli_price" id="oli_price" style="text-align: right;" readonly>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Telepon/Hp </label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="phone" id="phone" value="<?php echo $dataSupplier->phone; ?>" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Alamat </label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="address" id="address" value="<?php echo $dataSupplier->address; ?>" readonly>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="text-right">
        <button class="btn btn-danger" data-dismiss="modal"> Close</button>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#drum_price").val(accounting.formatMoney('<?php echo $drumPrice; ?>'));
        $("#oli_price").val(accounting.formatMoney('<?php echo $oliPrice; ?>'));
    });
</script>

