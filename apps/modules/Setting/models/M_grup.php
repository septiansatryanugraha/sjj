<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_grup extends CI_Model {

    public function getDataTables() {
        $data = $this->db->get('grup');
        return $data->result();
    }

    public function selectById($id) {
        $sql = "SELECT * FROM grup WHERE grup_id = '{$id}'";

        $data = $this->db->query($sql);

        return $data->row();
    }

    function simpan_data($data) {
        $result = $this->db->insert('grup', $data);
        return $result;
    }

    public function update($data, $where) {
        $result = $this->db->update('grup', $data, $where);
        return $result;
    }

    public function hapus($id) {
        $sql = "DELETE FROM grup WHERE grup_id ='" . $id . "'";

        $this->db->query($sql);

        return $this->db->affected_rows();
    }

}
