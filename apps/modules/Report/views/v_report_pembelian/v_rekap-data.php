<?php $this->load->view('_heading/_headerContent') ?>
<style>
    #report {
        max-width: 1200px;
        height: 330px;
        margin: 0 auto
    }   
</style>
<section class="content">
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <form method="post" action="<?php echo site_url('filter-pembelian'); ?>">
                <div class="box-header">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Tanggal Awal:</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="tanggal_awal" class="form-control" id="from" value="<?php echo date('01-m-Y'); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Tanggal Akhir:</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="tanggal_akhir" class="form-control" id="to" value="<?php echo date('t-m-Y'); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Supplier </label>
                            <div class="input-group col-sm-9">
                                <select name="supplier" class="form-control selek-tipe">
                                    <option value="">Semua Supplier</option>
                                    <?php foreach ($supplier as $data) { ?>
                                        <option value="<?php echo $data->id_supplier; ?>">
                                            <?php
                                            $idSupplier = $data->id_supplier;
                                            $dataSupplier = $this->M_rekap_pembelian->selectById($idSupplier);
                                            $namaSupplier = $dataSupplier->name;
                                            echo $namaSupplier;
                                            ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label></label>
                            <div class="input-group date">
                                <button name="simpan" type="submit" class="btn btn-sm btn-primary batas-export klik"><i class="fa fa-refresh"></i> Filter</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div id="report"></div>
    <!-- highchart -->
    <script src="<?php echo base_url(); ?>assets/plugins/highchart/highcharts.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/highchart/modules/exporting.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/highchart/modules/offline-exporting.js"></script>
    <!-- REPORT BARU -->
    <?php
    if (!empty($graph1)) {
        foreach ($graph1 as $data2) {
            $tanggal_beli[] = date('d-m-Y', strtotime($data2->created_date));
            $nominal_beli[] = (float) $data2->nominal;
        }
    } else {
        echo "Belum Ada data yang masuk";
    }
    ?>
    <!-- Script untuk memanggil library Highcharts -->
    <script>
        jQuery(function () {
            Highcharts.setOptions({
                lang: {
                    thousandsSep: '.'
                }
            });
            new Highcharts.Chart({
                chart: {
                    renderTo: 'report',
                    type: 'column',
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: 'Grafik pembelian',
                    x: -20
                },
                xAxis: {
                    categories: <?php echo json_encode($tanggal_beli); ?>
                },
                yAxis: {
                    labels: {
                        formatter: function () {
                            return IDRFormatter(this.value, 'Rp.');
                        }
                    },
                    title: {
                        text: 'Nominal'
                    }
                }, series: [{name: 'Nominal', data: <?php echo json_encode($nominal_beli); ?>}]
            });
        });
    </script>
</section>
<script>
    function IDRFormatter(angka, prefix) {
        var number_string = angka.toString().replace(/[^,\d]/g, ''),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
</script>
<script>
//klik loading ajax
    $(document).ready(function () {
        $('.klik').click(function () {
            var url = $(this).attr('href');
            $("#loading2").show().html("<img src='http://belanjaweb.com/sjj/assets/tambahan/gambar/loader-ok.gif' height='18'> ");
            $("#loading2").modal('show');
            $.ajax({
                complete: function () {
                    $("#loading2").hide();
                    $("#loading2").modal('hide');
                }
            });
            return true;
        });
    });
    // untuk datetime from
    $(function () {
        $("#from").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
    });
    // untuk datetime to
    $(function () {
        $("#to").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
    });
    $(function ()
    {
        $(".selek-tipe").select2({
            allowClear: true
        });
    });
</script>