<style>
.LastCol {
border: 1px solid black;    
}
.jarak{
  margin-right: 2px;
}
</style>



<?php 

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<center><h2>Report Data Pembelian PT Surabaya Jadi Jaya</h2></center>
<br>

<table>
<tr><td></td><td><b>Periode Report &nbsp;&nbsp;&nbsp; :</b></td><td><b>Periode awal &nbsp;&nbsp;&nbsp; : <?php echo date('d-m-Y',strtotime($tanggal_awal)) ?></b></td></tr>
</table>

<table>
<tr><td></td><td></td><td><b>Periode akhir &nbsp;&nbsp;&nbsp; : <?php echo date('d-m-Y',strtotime($tanggal_akhir)) ?></td></tr></table>
<br><br>

<table>
<tr><td></td><td><b>Pembelian Oli</b></td></tr>
</table><br>

<table border="1" width="60%">

   <thead>
   <tr>
   <th align="center">No</th>
   <th align="center">Tanggal Transaksi</th>
   <th align="center">Tanggal Pembayaran</th>
   <th align="center">Nama Supplier </th>
   <th align="center">Deskripsi Pembelian </th>
   <th align="center">Berat</th>
   <th align="center">Kadar Air</th>
   <th align="center"colspan="2">Qty / Satuan</th>
   <th align="center">Harga / Liter</th>
   <th align="center">Total Harga</th>
   </tr>
   </thead>

<tbody>
  <?php
  if (!empty($excel_oli)) {
  $no = 1;
  $harga_total_oli = 0;
  foreach ($excel_oli as $data) { 

     $idSupplier = $data->supplier;
            $dataSupplier = $this->M_rekap_pembelian->selectById($idSupplier);
            $namaSupplier = $dataSupplier->name;


    $tipe = 'Oli';
         
            if ($data->id_item > 1) {
                $tipe = 'Drum';
            }
            $harga_total_oli +=$data->grandtotal;
            $total_oli_last +=$data->qty;

          if ($data->paid==null) {
          $PaidTransaction = '';
          } else {
          $PaidTransaction= date('d-m-Y', strtotime($data->paid));
          }
    ?>
   <tr>
   <td><?php echo $no ?></td>
   <td><?php echo date('d-m-Y', strtotime($data->created_date));?>&nbsp;</td>
   <td><?php echo $PaidTransaction ?>&nbsp;</td>
   <td><?php echo $namaSupplier ?></td>
   <td><?php echo $data->deskripsi ?></td>
   <td align="left"><?php echo str_replace(".", ",", $data->specific_gravity) ?></td>
   <td align="left"><?php echo $data->water_content  ?></td>
   <td align="left"><?php echo number_format($data->qty, 0, ".", ".") ?></td>
   <td>liter</td>
   <td><?php echo number_format($data->total, 0, ".", ".") ?></td>
   <td><?php echo number_format($data->grandtotal, 0, ".", ".") ?></td>
   </tr>
  <?php $no++; } ?>
  <?php } else {  ?>
  <?php } ?>
   </tbody>
</table>
<table>

<tr style="border-bottom: 1px solid black"><td colspan="6"></td><td></td><td align="left"><?php echo number_format($total_oli_last, 0, ".", ".") ?></td><td>liter</td><td align="right"><b>Total </b></td><td class="LastCol"><b><?php echo number_format($harga_total_oli, 0, ".", ".") ?></b></td></tr></table>

<br><br>

<table>
<tr><td></td><td><b>Pembelian Drum</b></td></tr>
</table><br>
<table border="1" width="60%">

  <thead>
   <tr>
   <th align="center">No</th>
   <th align="center">Tanggal Transaksi</th>
   <th align="center">Nama Supplier </th>
   <th align="center">Deskripsi Pembelian </th>
    <th align="center"colspan="2">Qty / Satuan</th>
   <th align="center">Harga / Qty </th>
   <th align="center">Total Harga</th>
   </tr>
   </thead>

<tbody>
  <?php
  if (!empty($excel_drum)) {
  $no = 1;
  $harga_total_drum = 0;
  foreach ($excel_drum as $data) { 

    $idSupplier = $data->supplier;
            $dataSupplier = $this->M_rekap_pembelian->selectById($idSupplier);
            $namaSupplier = $dataSupplier->name;

    $tipe = 'Oli';
         
            if ($data->id_item > 1) {
                $tipe = 'Drum';
            }
            $harga_total_drum +=$data->grandtotal;
            $total_drum_last +=$data->qty;

            if ($data->paid==null) {
          $PaidTransaction = '';
          } else {
          $PaidTransaction= date('d-m-Y', strtotime($data->paid));
          }
    ?>
   <tr>
   <td><?php echo $no ?></td>
   <td><?php echo date('d-m-Y', strtotime($data->created_date));?>&nbsp;</td>
   <td><?php echo $PaidTransaction ?>&nbsp;</td>
   <td><?php echo $namaSupplier ?></td>
   <td><?php echo $data->deskripsi ?></td>
   <td align="left"><?php echo number_format($data->qty, 0, ".", ".") ?></td>
   <td>pcs</td>
   <td><?php echo number_format($data->total, 0, ".", ".") ?></td>
   <td><?php echo number_format($data->grandtotal, 0, ".", ".") ?></td>
   </tr>
  <?php $no++; } ?>
  <?php } else {  ?>
  <?php } ?>
   </tbody>
</table>
<table>

<tr style="border-bottom: 1px solid black"><td colspan="4" align="right"></td><td></td><td align="left"><?php echo number_format($total_drum_last, 0, ".", ".") ?></td><td>pcs</td><td align="right"><b>Total </b></td><td class="LastCol"><b><?php echo number_format($harga_total_drum, 0, ".", ".") ?></b></td></tr></table>

<br>

<table>
<?php $hasil= $harga_total_oli + $harga_total_drum; ?>

<tr style="border-bottom: 1px solid black; border-top:1px solid black;"><td colspan="8" align="right"><b>Total Pembelian </b></td><td class="LastCol"><b><?php echo number_format($hasil, 0, ".", ".") ?></b></td></tr></table>

<br>





