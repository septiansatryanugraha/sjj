<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_laba_rugi extends AUTH_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('M_rekap_laba_rugi');
        $this->load->model('M_rekap_penjualan');
        $this->load->model('M_rekap_pembelian');
        $this->load->model('M_sidebar');
    }

    public function loadkonten($page, $data) {

        $data['userdata'] = $this->userdata;
        $ajax = ($this->input->post('status_link') == "ajax" ? true : false);
        if (!$ajax) {
            $this->load->view('Dashboard/layouts/header', $data);
        }
        $this->load->view($page, $data);
        if (!$ajax)
            $this->load->view('Dashboard/layouts/footer', $data);
    }

    public function index() {
        $data['userdata'] = $this->userdata;
        $data['page'] = "Report Laba & Rugi";
        $data['judul'] = "Report Laba & Rugi";

        $this->loadkonten('v_report_laba_rugi/v_rekap-data', $data);
    }

    // 	public function filter(){
    // 	$data['userdata'] 	= $this->userdata;  
    // 	$data['page'] 		= "Report Laba & Rugi";
    // 	$data['judul'] 		= "Report Laba & Rugi";
    // 	$tanggal_awal=$this->input->post('tanggal_awal');
    // 	$tanggal_akhir=$this->input->post('tanggal_akhir');
    // 	$data['filter']  = $this->M_rekap_laba_rugi->export_data($tanggal_awal,$tanggal_akhir);
    // 	$data['tanggal_awal']=$tanggal_awal;
    // 	$data['tanggal_akhir']=$tanggal_akhir;
    //     $this->loadkonten('v_report_penjualan/filter',$data);
    // }

    public function export_excel() {

        $waktu = date("Y-m-d h:i");

        $data['title'] = "Report Data Laba & Rugi PT Surabaya Jadi Jaya " . $waktu . "";

        $tanggal_awal = $this->input->post('tanggal_awal');
        $tanggal_akhir = $this->input->post('tanggal_akhir');


        $data['penjualan_oli'] = $this->M_rekap_laba_rugi->export_penjualan_oli($tanggal_awal, $tanggal_akhir);
        $data['penjualan_drum'] = $this->M_rekap_laba_rugi->export_penjualan_drum($tanggal_awal, $tanggal_akhir);
        $data['pembelian_oli'] = $this->M_rekap_laba_rugi->export_pembelian_oli($tanggal_awal, $tanggal_akhir);
        $data['pembelian_drum'] = $this->M_rekap_laba_rugi->export_pembelian_drum($tanggal_awal, $tanggal_akhir);

        $sum_penjualan_oli = $this->M_rekap_laba_rugi->sum_penjualan_oli();
        $sum_penjualan_drum = $this->M_rekap_laba_rugi->sum_penjualan_drum();
        $sum_pembelian_oli = $this->M_rekap_laba_rugi->sum_pembelian_oli();
        $sum_pembelian_drum = $this->M_rekap_laba_rugi->sum_pembelian_drum();



        foreach ($sum_penjualan_oli as $data_jual_oli) {
            $sum_penjualan_oli = $data_jual_oli->total_penjualan_oli;
        }

        $data['sum_penjualan_oli'] = $sum_penjualan_oli;


        foreach ($sum_penjualan_drum as $data_jual_drum) {
            $sum_penjualan_drum = $data_jual_drum->total_penjualan_drum;
        }

        $data['sum_penjualan_drum'] = $sum_penjualan_drum;

        foreach ($sum_pembelian_oli as $data_beli_oli) {
            $sum_pembelian_oli = $data_beli_oli->total_pembelian_oli;
        }

        $data['sum_pembelian_oli'] = $sum_pembelian_oli;

        foreach ($sum_pembelian_drum as $data_beli_drum) {
            $sum_pembelian_drum = $data_beli_drum->total_pembelian_drum;
        }

        $data['sum_pembelian_drum'] = $sum_pembelian_drum;

        $data['tanggal_awal'] = $tanggal_awal;
        $data['tanggal_akhir'] = $tanggal_akhir;
        $this->load->view('v_report_laba_rugi/v_laporan_excel', $data);
    }

    // public function tes ()
    // {
    // 	$sum_oli           = $this->M_rekap_stock->sum_oli();
    // 	$sum_drum          = $this->M_rekap_stock->sum_drum();
    // 	foreach ($sum_oli as $data_sum) {
    // 	$sum_oli_in = $data_sum->stok_masuk;
    // 	$sum_oli_out = $data_sum->stok_keluar;
    // 	}
    // 	$data['sum_oli_in']=$sum_oli_in;
    // 	$data['sum_oli_out']=$sum_oli_out;
    // 	foreach ($sum_drum as $data_sum2) {
    // 	$sum_drum_in = $data_sum->stok_masuk;
    // 	$sum_drum_out = $data_sum->stok_keluar;
    // 	}
    // 	$data['sum_drum_in']=$sum_drum_in;
    // 	$data['sum_drum_out']=$sum_drum_out;
    // 	echo $sum_drum_in;
    // }
}
