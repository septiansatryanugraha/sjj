<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_rekap_laba_rugi extends CI_Model {

    const __tableName = 'tbl_customer';
    const __tableId = 'id_customer';
    const __tableName2 = 'tbl_supplier';
    const __tableId2 = 'id_supplier';

    public function export_penjualan_oli($tanggalAwal, $tanggalAkhir) {
        if (strlen($tanggalAwal) > 0) {
            $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
        }
        if (strlen($tanggalAkhir) > 0) {
            $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
        }

        $fixed = 0;
        $tipe = 1;
        $this->db->select('tbl_transaction_sales_detail.id_transaction_sales_detail as id_transaction_sales_detail,	
                            tbl_transaction_sales_detail.id_item as id_item,
                            tbl_transaction_sales_detail.qty as qty,
                            tbl_transaction_sales_detail.total as total,
                            tbl_transaction_sales_detail.grandtotal as grandtotal,
                            tbl_transaction_sales.id_customer as customer,

                            tbl_transaction_sales_detail.created_date As created_date,
                            tbl_transaction_sales_detail.created_by As created_by,');

        $this->db->join('tbl_transaction_sales', 'tbl_transaction_sales_detail.id_transaction_sales = tbl_transaction_sales.id_transaction_sales', 'inner');

        $this->db->where('tbl_transaction_sales_detail.created_date >=', $tanggalAwal);
        $this->db->where('tbl_transaction_sales_detail.created_date <=', $tanggalAkhir);
        $this->db->like('tbl_transaction_sales_detail.id_item', $tipe);


        // $this->db->where("(master_data.tanggal >='$tanggalAwal' AND master_data.tanggal<='$tanggalAkhir')");
        $query = $this->db->get('tbl_transaction_sales_detail');
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    public function export_penjualan_drum($tanggalAwal, $tanggalAkhir) {
        if (strlen($tanggalAwal) > 0) {
            $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
        }
        if (strlen($tanggalAkhir) > 0) {
            $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
        }

        $fixed = 0;
        $tipe = 2;
        $this->db->select('tbl_transaction_sales_detail.id_transaction_sales_detail as id_transaction_sales_detail,	
                            tbl_transaction_sales_detail.id_item as id_item,
                            tbl_transaction_sales_detail.qty as qty,
                            tbl_transaction_sales_detail.total as total,
                            tbl_transaction_sales_detail.grandtotal as grandtotal,
                            tbl_transaction_sales.id_customer as customer,

                            tbl_transaction_sales_detail.created_date As created_date,
                            tbl_transaction_sales_detail.created_by As created_by,');

        $this->db->join('tbl_transaction_sales', 'tbl_transaction_sales_detail.id_transaction_sales = tbl_transaction_sales.id_transaction_sales', 'inner');

        $this->db->where('tbl_transaction_sales_detail.created_date >=', $tanggalAwal);
        $this->db->where('tbl_transaction_sales_detail.created_date <=', $tanggalAkhir);
        $this->db->like('tbl_transaction_sales_detail.id_item', $tipe);

        // $this->db->where("(master_data.tanggal >='$tanggalAwal' AND master_data.tanggal<='$tanggalAkhir')");
        $query = $this->db->get('tbl_transaction_sales_detail');
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    public function sum_penjualan_oli() {

        $fixed = 0;
        $tipe = 1;

        $this->db->select('sum(grandtotal) as total_penjualan_oli');

        $this->db->like('tbl_transaction_sales_detail.id_item', $tipe);

        // $this->db->where("(master_data.tanggal >='$tanggalAwal' AND master_data.tanggal<='$tanggalAkhir')");
        $query = $this->db->get('tbl_transaction_sales_detail');
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    public function sum_penjualan_drum() {

        $fixed = 0;
        $tipe = 2;

        $this->db->select('sum(grandtotal) as total_penjualan_drum');


        $this->db->like('tbl_transaction_sales_detail.id_item', $tipe);

        // $this->db->where("(master_data.tanggal >='$tanggalAwal' AND master_data.tanggal<='$tanggalAkhir')");
        $query = $this->db->get('tbl_transaction_sales_detail');
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    public function export_pembelian_oli($tanggalAwal, $tanggalAkhir) {
        if (strlen($tanggalAwal) > 0) {
            $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
        }
        if (strlen($tanggalAkhir) > 0) {
            $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
        }

        $fixed = 0;
        $tipe = 1;
        $this->db->select('tbl_transaction_purchase_detail.id_transaction_purchase_detail as id_transaction_purchase_detail,	
                            tbl_transaction_purchase_detail.id_item as id_item,
                            tbl_transaction_purchase_detail.qty as qty,
                            tbl_transaction_purchase_detail.depreciation as depreciation,
                            tbl_transaction_purchase_detail.total as total,
                            tbl_transaction_purchase_detail.grandtotal as grandtotal,
                            tbl_transaction_purchase.id_supplier as supplier,

                            tbl_transaction_purchase_detail.created_date As created_date,
                            tbl_transaction_purchase_detail.created_by As created_by,');

        $this->db->join('tbl_transaction_purchase', 'tbl_transaction_purchase_detail.id_transaction_purchase = tbl_transaction_purchase.id_transaction_purchase', 'inner');

        $this->db->where('tbl_transaction_purchase_detail.created_date >=', $tanggalAwal);
        $this->db->where('tbl_transaction_purchase_detail.created_date <=', $tanggalAkhir);
        $this->db->like('tbl_transaction_purchase_detail.id_item', $tipe);


        // $this->db->where("(master_data.tanggal >='$tanggalAwal' AND master_data.tanggal<='$tanggalAkhir')");
        $query = $this->db->get('tbl_transaction_purchase_detail');
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    public function export_pembelian_drum($tanggalAwal, $tanggalAkhir) {
        if (strlen($tanggalAwal) > 0) {
            $tanggalAwal = date('Y-m-d H:i:s', strtotime($tanggalAwal . ' 00:00:00'));
        }
        if (strlen($tanggalAkhir) > 0) {
            $tanggalAkhir = date('Y-m-d H:i:s', strtotime($tanggalAkhir . ' 23:58:59'));
        }

        $fixed = 0;
        $tipe = 2;
        $this->db->select('tbl_transaction_purchase_detail.id_transaction_purchase_detail as id_transaction_purchase_detail,	
                            tbl_transaction_purchase_detail.id_item as id_item,
                            tbl_transaction_purchase_detail.qty as qty,
                            tbl_transaction_purchase_detail.depreciation as depreciation,
                            tbl_transaction_purchase_detail.total as total,
                            tbl_transaction_purchase_detail.grandtotal as grandtotal,
                            tbl_transaction_purchase.id_supplier as supplier,

                            tbl_transaction_purchase_detail.created_date As created_date,
                            tbl_transaction_purchase_detail.created_by As created_by,');

        $this->db->join('tbl_transaction_purchase', 'tbl_transaction_purchase_detail.id_transaction_purchase = tbl_transaction_purchase.id_transaction_purchase', 'inner');

        $this->db->where('tbl_transaction_purchase_detail.created_date >=', $tanggalAwal);
        $this->db->where('tbl_transaction_purchase_detail.created_date <=', $tanggalAkhir);
        $this->db->like('tbl_transaction_purchase_detail.id_item', $tipe);


        // $this->db->where("(master_data.tanggal >='$tanggalAwal' AND master_data.tanggal<='$tanggalAkhir')");
        $query = $this->db->get('tbl_transaction_purchase_detail');
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    public function sum_pembelian_oli() {

        $fixed = 0;
        $tipe = 1;

        $this->db->select('sum(grandtotal) as total_pembelian_oli');

        $this->db->like('tbl_transaction_purchase_detail.id_item', $tipe);

        // $this->db->where("(master_data.tanggal >='$tanggalAwal' AND master_data.tanggal<='$tanggalAkhir')");
        $query = $this->db->get('tbl_transaction_purchase_detail');
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    public function sum_pembelian_drum() {

        $fixed = 0;
        $tipe = 2;

        $this->db->select('sum(grandtotal) as total_pembelian_drum');


        $this->db->like('tbl_transaction_purchase_detail.id_item', $tipe);

        // $this->db->where("(master_data.tanggal >='$tanggalAwal' AND master_data.tanggal<='$tanggalAkhir')");
        $query = $this->db->get('tbl_transaction_purchase_detail');
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    public function selectByIdsupplier($id) {
        $sql = "SELECT * FROM " . self::__tableName2 . " WHERE " . self::__tableId2 . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

    public function selectByIdcustomer($id) {
        $sql = "SELECT * FROM " . self::__tableName . " WHERE " . self::__tableId . " = '{$id}'";
        $data = $this->db->query($sql);
        return $data->row();
    }

}
