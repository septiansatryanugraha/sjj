<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_total extends CI_Model {

    public function totalCustomer() {
        $data = $this->db->get('tbl_customer');
        return $data->num_rows();
    }

    public function totalSupplier() {
        $data = $this->db->get('tbl_supplier');
        return $data->num_rows();
    }

    public function totalVehicle() {
        $data = $this->db->get('tbl_vehicle');
        return $data->num_rows();
    }

    public function transaksiPeminjaman() {
        $data = $this->db->get('tbl_supplier_loan');
        return $data->num_rows();
    }

    public function transaksiPenyewaan() {
        $data = $this->db->get('tbl_vehicle_rent');
        return $data->num_rows();
    }

    public function transaksiPembelian() {
        $data = $this->db->get('tbl_transaction_purchase');
        return $data->num_rows();
    }

    public function transaksiPenjualan() {
        $data = $this->db->get('tbl_transaction_sales');
        return $data->num_rows();
    }

    public function transaksiMaintenance() {
        $data = $this->db->get('tbl_vehicle_maintenance');
        return $data->num_rows();
    }

    public function getTotalPembelian() {
        $query = $this->db->query("SELECT created_date,sum(nominal) AS nominal FROM tbl_transaction_purchase WHERE created_date between DATE_ADD(date(now()), INTERVAL -20 DAY) and date(now()) GROUP BY DATE(created_date)");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

    public function getTotalPenjualan() {
        $query = $this->db->query("SELECT created_date,sum(nominal) AS nominal FROM tbl_transaction_sales WHERE created_date between DATE_ADD(date(now()), INTERVAL -20 DAY) and date(now()) GROUP BY DATE(created_date)");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $data) {
                $hasil[] = $data;
            }
            return $hasil;
        }
    }

}
