<?php $this->load->view('_heading/_headerContent') ?>
<section class="content">
    <div class="box">
        <div class="box-header">
            <div class="col-md-2" style="margin-left: -20px; margin-top:-5px;">
                <?php if ($accessAdd > 0) { ?>
                    <div class="box-header with-border">
                        <a class="klik" href="<?php echo site_url('add-' . $menuName); ?>"><button class="btn btn-success" ><i class="glyphicon glyphicon-plus-sign"></i> Tambah <?php echo $judul; ?></button></a>
                    </div>
                <?php } ?>
            </div>
            <br><br><br>
            <div class="search-form" style="">
                <div class="form-group ">
                    <label class="control-label">Filter</label>
                </div>
                <div class="form-group ">
                    <label class="col-sm-2 control-label">Tanggal Pemeliharaan</label>
                    <div class="col-sm-6">
                        <input type="text" name="tanggal_awal" id="tanggal_awal" class="form-control datepicker" style="width: 30%; display: inline-block;" value="<?php echo date('01-m-Y'); ?>" readonly="">
                        <span>s/d</span>
                        <input type="text" name="tanggal_akhir" id="tanggal_akhir" class="form-control datepicker" style="width: 30%; display: inline-block;" value="<?php echo date('t-m-Y'); ?>" readonly="">
                    </div>
                    <div style="clear:both"></div>
                </div>
                <div class="form-group">  
                    <label class="col-sm-2 control-label"></label>       
                    <div class="col-sm-6">
                        <label class="checkbox-inline">
                            <input type="checkbox" id="all_date" name="all_date"/>Semua Tanggal
                        </label>
                    </div>
                    <div style="clear:both"></div>
                </div>
                <div class="box-footer">
                    <button name="button_filter" id="button_filter" style="margin-top: 13px" type="button" class="btn btn-success btn-flat"><i class="fa fa-refresh"></i> Tampilkan</button>
                </div>
                <div class="box-footer"><br></div>
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table id="table_data" class="table table-data table-striped table-bordered display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Armada</th>
                            <th>Tanggal</th>
                            <th>Biaya</th>
                            <th>Keterangan</th>
                            <th width="70px">Di Buat</th>
                            <th width="70px">Di Ubah</th>
                            <th width="50px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    // untuk datepicker
    $(function () {
        $(".datepicker").datepicker({
            orientation: "left",
            autoclose: !0,
            format: 'dd-mm-yyyy'
        })
    });

    //untuk load data table ajax	
    var save_method; //for save method string
    var table_data;

    $(document).ready(function () {
        reloadTable();
    });

    function reloadTable() {
        var tanggal_awal = $("#tanggal_awal").val();
        var tanggal_akhir = $("#tanggal_akhir").val();
        var all_date = 0;
        if (document.getElementById("all_date").checked == true) {
            all_date = 1;
        }

        table_data = $('#table_data').DataTable({
            "aLengthMenu": [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
            "pageLength": 25,
            "processing": true, //Feature control the processing indicator.
            // "scrollX": true,
            "order": [], //Initial no order.
            oLanguage: {
                "sProcessing": "<img src='<?php base_url(); ?>assets/tambahan/gambar/loading.gif' width='25px'>",
                "sLengthMenu": "_MENU_ &nbsp;&nbsp;Data Per Halaman",
                "sInfo": "Menampilkan _START_ s/d _END_ dari <b>_TOTAL_ data</b>",
                "sInfoFiltered": "(difilter dari _MAX_ total data)",
                "sEmptyTable": "No data found in the server",
                "sInfoPostFix": "",
                "sSearch": "<i class='fa fa-search fa-fw'></i> Pencarian : ",
                "sPaginationType": "simple_numbers",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('Transaction/VehicleMaintenance/ajaxList') ?>",
                "type": "POST",
                data: {tanggal_awal: tanggal_awal, tanggal_akhir: tanggal_akhir, all_date: all_date},
            },
            //Set column definition initialisation properties.
            "columnDefs": [{
                    "targets": [-1], //last column
                    "orderable": false, //set not orderable
                },
            ],
        });
    }

    $('#search-button').click(function () {
        $('.search-form').toggle();
        return false;
    });

    $("#button_filter").click(function () {
        table_data.destroy();
        reloadTable();
    });

    $(document).on("click", ".hapus-pemeliharaan", function () {
        var error = 0;
        var message = "";

        var id_vehicle_maintenance = $(this).attr("data-id");

        swal({
            title: "Hapus Data?",
            text: "Yakin anda akan menghapus data ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Hapus",
            confirmButtonColor: '#dc1227',
            customClass: ".sweet-alert button",
            closeOnConfirm: false,
            html: true
        },
        function () {
            $(".confirm").attr('disabled', 'disabled');
            $.ajax({
                method: "POST",
                url: "<?php echo base_url('Transaction/VehicleMaintenance/prosesDelete'); ?>",
                data: "id_vehicle_maintenance=" + id_vehicle_maintenance,
                success: function (data) {
                    var result = jQuery.parseJSON(data);
                    if (result.status == 'berhasil') {
                        $("tr[data-id='" + id_vehicle_maintenance + "']").fadeOut("fast", function () {
                            $(this).remove();
                        });
                        $(".loading2").hide();
                        $(".loading2").modal('hide');
                        hapus_berhasil();
                        table_data.destroy();
                        reloadTable();
                        setTimeout("window.location='<?php echo site_url('Transaction/VehicleMaintenance'); ?>'", 450);
                    } else {
                        $(".loading2").hide();
                        $(".loading2").modal('hide');
                        swal("Peringatan", result.status, "warning");
                    }
                }
            });
        });
    });

</script>