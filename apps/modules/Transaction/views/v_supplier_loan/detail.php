<div class="col-md-12 well">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h3 style="display:block; text-align:center;"><i class="fa fa-location-arrow"></i> Detail <?php echo $judul; ?></h3>
    <div class="box box-body">
        <div class="form-horizontal">
            <div class="box-body">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Supplier </label>
                    <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="supplier"></label></div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Nominal </label>
                    <div class="col-sm-3">: <label for="inputEmail3" class="control-label" id="nominal"></label></div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Telah dibayar </label>
                    <div class="col-sm-3">: <label for="inputEmail3" class="control-label" id="paid"></label></div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Belum dibayar </label>
                    <div class="col-sm-3">: <label for="inputEmail3" class="control-label" id="debt"></label></div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Status </label>
                    <div class="col-sm-3">: <label for="inputEmail3" class="control-label" id="status"></label></div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Keterangan </label>
                    <div class="col-sm-5">: <label for="inputEmail3" class="control-label" id="description"><?php echo $dataSupplierLoan->description; ?></label></div>
                </div>
            </div>
            <?php if (count($arrInstallment) > 0) { ?>
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Cicilan Lunas </label>
                    </div>
                </div>
                <div class="box-body">
                    <table id="tabel-detail" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Cicilan ke</th>
                                <th>Deskripsi</th>
                                <th>Nominal</th>
                                <th>Di Input</th>
                                <th>Di Input Oleh</th>
                            </tr>
                        </thead>
                        <tbody id="data-peminjaman">
                            <?php
                            $no = 1;
                            foreach ($arrInstallment as $value) {
                                ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $value['description']; ?></td>
                                    <td style="text-align: right;"><?php echo number_format($value['nominal'], 0, ",", "."); ?></td>
                                    <td><?php echo date('d-m-Y H:i:s', strtotime($value['created_date'])); ?></td>
                                    <td><?php echo $value['created_by']; ?></td>
                                </tr>
                                <?php
                                $no++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            <?php } ?>
            <?php if (count($arrInstallmentPending) > 0) { ?>
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Cicilan Pending </label>
                    </div>
                </div>
                <div class="box-body">
                    <table id="tabel-detail" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Cicilan ke</th>
                                <th>Deskripsi</th>
                                <th>Nominal</th>
                                <th>Di Input</th>
                                <th>Di Input Oleh</th>
                            </tr>
                        </thead>
                        <tbody id="data-peminjaman">
                            <?php
                            $no = 1;
                            foreach ($arrInstallmentPending as $value) {
                                ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo $value['description']; ?></td>
                                    <td style="text-align: right;"><?php echo number_format($value['nominal'], 0, ",", "."); ?></td>
                                    <td><?php echo date('d-m-Y H:i:s', strtotime($value['created_date'])); ?></td>
                                    <td><?php echo $value['created_by']; ?></td>
                                </tr>
                                <?php
                                $no++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="text-right">
        <button class="btn btn-danger" data-dismiss="modal"> Close</button>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#supplier").html('<?php echo $namaSupplier; ?>');
        $("#nominal").html(accounting.formatMoney('<?php echo $dataSupplierLoan->nominal; ?>'));
        $("#paid").html(accounting.formatMoney('<?php echo $dataSupplierLoan->paid; ?>'));
        $("#debt").html(accounting.formatMoney('<?php echo $dataSupplierLoan->debt; ?>'));
        $("#status").html('<?php echo $statusLunas; ?>');
    });
</script>